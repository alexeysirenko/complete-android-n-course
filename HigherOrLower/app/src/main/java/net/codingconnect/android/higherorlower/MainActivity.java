package net.codingconnect.android.higherorlower;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private int number = 0;

    private int lowest = 1;

    private int highest = 10;

    private Random random = new Random();

    private TextView guessTextView = null;

    private EditText guessEditText = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        guessEditText = (EditText) findViewById(R.id.guessEditText);
        guessTextView = (TextView) findViewById(R.id.guessTextView);
        number = nextNumber(lowest, highest);
        Log.d("number", String.valueOf(number));
        updateView(lowest, highest);
    }

    public void guessButtonClick(View view) {
        try {
            int maybeNumber = Integer.parseInt(guessEditText.getText().toString().trim());
            Log.d("numbers", maybeNumber + " / " + String.valueOf(number));
            if (maybeNumber == number) {
                Toast.makeText(this, "Correct! " + maybeNumber + " is the right answer" , Toast.LENGTH_LONG).show();
                number = nextNumber(lowest, highest);
                updateView(lowest, highest);
            } else if (maybeNumber > number) {
                Toast.makeText(this, "Less" , Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "More" , Toast.LENGTH_SHORT).show();
            }

        } catch (NumberFormatException e) {
            Toast.makeText(this, "Invalid number", Toast.LENGTH_SHORT).show();
        }
    }

    private int nextNumber(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    private void updateView(int min, int max) {
        guessTextView.setText(String.format(getString(R.string.guess), min, max));

    }
}
