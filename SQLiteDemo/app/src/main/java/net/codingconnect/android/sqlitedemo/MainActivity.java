package net.codingconnect.android.sqlitedemo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Result - event", "Starting...");

        SQLiteDatabase eventsDB = this.openOrCreateDatabase("events", MODE_PRIVATE, null);
        try {
            eventsDB.execSQL("CREATE TABLE IF NOT EXISTS events (event VARCHAR, year INT(4))");

            eventsDB.execSQL("INSERT INTO events (event, year) VALUES ('End of WW2', 1945)");

            eventsDB.execSQL("INSERT INTO events (event, year) VALUES ('Some other date', 1986)");

            Cursor c = eventsDB.rawQuery("SELECT event, year FROM events", null);

            int eventIndex = c.getColumnIndexOrThrow("event");
            int yearIndex = c.getColumnIndexOrThrow("year");

            c.moveToFirst();
            while (c.moveToNext()) {
                Log.i("Result - event", c.getString(eventIndex));
                Log.i("Result - event", String.valueOf(c.getInt(yearIndex)));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            eventsDB.close();
        }

    }
}
