package net.codingconnect.android.eggtimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private Button startButton;
    private SeekBar seekBar;
    private TextView timeTextView;

    private CountDownTimer timer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = (Button) findViewById(R.id.startButton);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        timeTextView = (TextView) findViewById(R.id.timeTextView);
        seekBar.setMax(45 * 60 * 1000);
        seekBar.setProgress(15 * 60 * 1000);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser && progress < 1) {
                    seekBar.setProgress(1);
                } else {
                    displayTime(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        stopTimer();
    }

    public void onStartButtonClick(View view) {
        if (timer != null) {
            stopTimer();
        } else {
            startTimer(seekBar.getProgress());
        }
    }

    private void startTimer(long interval) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(interval, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                displayTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                playSound();
                stopTimer();
            }
        };
        startButton.setText(R.string.stop);
        seekBar.setEnabled(false);
        timer.start();
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = null;
        displayTime(seekBar.getProgress());
        seekBar.setEnabled(true);
        startButton.setText(R.string.start);
    }

    private void displayTime(long millisUntilFinished) {
        String text = String.format("%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
        );
        timeTextView.setText(text);
    }

    private void playSound() {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
        mediaPlayer.start();
    }
}
