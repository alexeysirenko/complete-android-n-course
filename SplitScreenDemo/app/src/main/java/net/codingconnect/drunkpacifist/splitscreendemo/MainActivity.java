package net.codingconnect.drunkpacifist.splitscreendemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (isInMultiWindowMode()) {
            // We are in multi window mode
        }

        if (isInPictureInPictureMode()) {
            // We're in picture in picture mode
        }
    }

    @Override
    public void onMultiWindowModeChanged(boolean isMulti) {
        super.onMultiWindowModeChanged(isMulti);

        /// TODO
    }
}
