package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {

    String recipientUsername = null;

    EditText chatEditText;

    ArrayList<Message> messages = new ArrayList<>();

    ListView chatListView;

    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        chatEditText = (EditText) findViewById(R.id.chatEditText);

        Intent intent = getIntent();
        recipientUsername = intent.getStringExtra("recipient");
        Log.i("Chat", recipientUsername);

        setTitle("Chat with " + recipientUsername);

        chatListView = (ListView) findViewById(R.id.chatListView);

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, messages);

        chatListView.setAdapter(arrayAdapter);

        checkMessages();



    }

    public void onSendButtonClick(View v) {
        ParseObject message = new ParseObject("Message");
        message.put("sender", ParseUser.getCurrentUser().getUsername());
        message.put("recipient", recipientUsername);
        message.put("message", chatEditText.getText().toString().trim());
        chatEditText.getText().clear();
        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("Message", "Sent");
                    checkMessages();
                } else {
                    Log.e("Message", e.getMessage());
                }
            }
        });

    }

    private void checkMessages() {
        ParseQuery<ParseObject> queryOurs = new ParseQuery<>("Message");
        queryOurs.whereEqualTo("sender", ParseUser.getCurrentUser().getUsername());
        queryOurs.whereEqualTo("recipient", recipientUsername);

        ParseQuery<ParseObject> queryTheirs = new ParseQuery<>("Message");
        queryTheirs.whereEqualTo("sender", recipientUsername);
        queryTheirs.whereEqualTo("recipient", ParseUser.getCurrentUser().getUsername());

        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(queryOurs);
        queries.add(queryTheirs);

        ParseQuery<ParseObject> query = ParseQuery.or(queries);
        query.orderByAscending("createdAt");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    messages.clear();
                    for (ParseObject message: objects) {
                        String messageContent = message.getString("message");
                        if (!message.getString("sender").equals(ParseUser.getCurrentUser().getUsername())) {
                            messageContent = "> " + messageContent;
                        }
                        messages.add(new Message(message.getString("sender"), messageContent));
                    }
                    arrayAdapter.notifyDataSetChanged();
                } else {
                    Log.e("Check messages", e.getMessage());
                }
            }
        });
    }

    private class Message {

        String sender;
        String text;

        public Message(String sender, String text) {
            this.sender = sender;
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

    }
}
