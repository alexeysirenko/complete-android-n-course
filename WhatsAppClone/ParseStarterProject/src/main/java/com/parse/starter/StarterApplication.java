/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class StarterApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    // Enable Local Datastore.
    Parse.enableLocalDatastore(this);

    // Add your initialization code here
    Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
            .applicationId("5376cb1ded75bec7a01b22574bb159da79cc79f5")
            .clientKey("8dab551dcd3eb2a72c98d8eff85d4474dc765c2b")
            .server("http://ec2-35-176-24-222.eu-west-2.compute.amazonaws.com:80/parse/")
            .build()
    );

    /*
    *
    * databaseURI: "mongodb://root:XHV4xROMGLkB@127.0.0.1:27017/bitnami_parse",
    cloud: "./node_modules/parse-server/lib/cloud-code/Parse.Cloud.js",
    appId: "5376cb1ded75bec7a01b22574bb159da79cc79f5",
    masterKey: "8dab551dcd3eb2a72c98d8eff85d4474dc765c2b",
    fileKey: "4e655b1300b1db53aa53c45c13da7829463c0185",
    serverURL: "http://ec2-35-176-24-222.eu-west-2.compute.amazonaws.com:80/parse"

    XHV4xROMGLkB

    * */

    /*ParseObject object = new ParseObject("ExampleObject");
    object.put("myNumber", "123");
    object.put("myString", "rob");

    object.saveInBackground(new SaveCallback () {
      @Override
      public void done(ParseException ex) {
        if (ex == null) {
          Log.i("Parse Result", "Successful!");
        } else {
          Log.i("Parse Result", "Failed" + ex.toString());
        }
      }
    });
*/

    //ParseUser.enableAutomaticUser();

    ParseACL defaultACL = new ParseACL();
    defaultACL.setPublicReadAccess(true);
    defaultACL.setPublicWriteAccess(true);
    ParseACL.setDefaultACL(defaultACL, true);

  }
}
