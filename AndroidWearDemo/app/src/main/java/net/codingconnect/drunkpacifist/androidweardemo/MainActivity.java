package net.codingconnect.drunkpacifist.androidweardemo;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                if (findViewById(R.id.rect_layout) != null) {
                    Log.i("Info", "Rectangular");
                } else {
                    Log.i("Info", "Round");
                }

                FragmentManager fragmentManager = getFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                CardFragment cardFragment = CardFragment.create("Hello",
                        "How are you",
                        android.R.drawable.btn_plus);

                fragmentTransaction.add(R.id.frame_layout, cardFragment);

                fragmentTransaction.commit();
            }
        });


    }
}
