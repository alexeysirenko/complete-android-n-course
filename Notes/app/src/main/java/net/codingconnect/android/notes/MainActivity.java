package net.codingconnect.android.notes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<String> notes = new ArrayList<>();

    private final int ADD_NOTE_RESULT = 1;

    private final int EDIT_NOTE_RESULT = 2;

    ArrayAdapter notesAdapter;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        load(notes);

        ListView listView = (ListView) findViewById(R.id.listView);

        notesAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, notes);

        listView.setAdapter(notesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
                intent.putExtra("note", notes.get(position));
                intent.putExtra("position", position);
                startActivityForResult(intent, EDIT_NOTE_RESULT);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Remove")
                        .setMessage("Remove this item?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                notes.remove(position);
                                notesAdapter.notifyDataSetChanged();
                                save(notes);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();

                return true;
            }
        });
    }

    public void onAddButtonClick(View v) {
        Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
        startActivityForResult(intent, ADD_NOTE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            String note = data.getStringExtra("note");
            if (note != null) {
                if (requestCode == EDIT_NOTE_RESULT) {
                    int position = data.getIntExtra("position", -1);
                    if (position >= 0) {
                        notes.set(position, note);
                    }
                } else {
                    if (!note.trim().isEmpty()) {
                        notes.add(0, note);
                    }
                }
                notesAdapter.notifyDataSetChanged();
                save(notes);
            }
        }
    }

    private void save(ArrayList<String> notes) {
        if (sharedPreferences == null) {
            sharedPreferences = this.getSharedPreferences("foo.bar.Notes", Context.MODE_PRIVATE);
        }
        JSONArray jsonArray = new JSONArray(notes);
        sharedPreferences.edit().putString("notes", jsonArray.toString()).apply();
    }

    private void load(ArrayList<String> notes) {
        if (sharedPreferences == null) {
            sharedPreferences = this.getSharedPreferences("foo.bar.Notes", Context.MODE_PRIVATE);
        }
        try {
            JSONArray arr = new JSONArray(sharedPreferences.getString("notes", "[]"));
            notes.clear();
            for (int i = 0; i < arr.length(); i++) {
                notes.add(arr.get(i).toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
