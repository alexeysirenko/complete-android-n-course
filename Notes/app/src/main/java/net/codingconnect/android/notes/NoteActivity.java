package net.codingconnect.android.notes;

import android.content.Intent;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class NoteActivity extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        editText = (EditText) findViewById(R.id.editText);

        Intent intent = getIntent();

        String existingNote = intent.getStringExtra("note");
        if (existingNote != null) {
            editText.setText(existingNote);
        }
    }

    public void onBackPressed() {
        Intent intent = getIntent();
        String note = editText.getText().toString();
        intent.putExtra("note", note);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }
}
