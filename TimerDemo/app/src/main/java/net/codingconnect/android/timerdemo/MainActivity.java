package net.codingconnect.android.timerdemo;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Timer way 1
        final Handler handler = new Handler();
        Runnable run = new Runnable() {
            @Override
            public void run() {
               // Will be executed every second
               Log.i("Handler timer", "every second");

               handler.postDelayed(this, 1000);
            }
        };
        handler.post(run);

        new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.i("CountDownTimer timer", Long.toString(millisUntilFinished) + " ms until done");

            }

            @Override
            public void onFinish() {
                Log.i("CountDownTimer timer", "finished");
            }
        }.start();
    }
}
