package net.codingconnect.android.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fade(View v) {
        ImageView homerImage = (ImageView) findViewById(R.id.homerImage);
        ImageView bartImage = (ImageView) findViewById(R.id.bartImage);


        bartImage.animate().alpha(0f).setDuration(2000);
        homerImage.animate().alpha(1f).setDuration(2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
