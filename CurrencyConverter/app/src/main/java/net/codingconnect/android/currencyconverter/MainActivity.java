package net.codingconnect.android.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void convertClick(View view) {

        EditText amountEditText = (EditText) findViewById(R.id.amountEditText);

        try {
            Double amount = Double.parseDouble(amountEditText.getText().toString().trim());

            Double convertedAmount = amount * 27;

            Toast.makeText(this, String.format("%.2f", convertedAmount), Toast.LENGTH_SHORT).show();
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Invalid amount", Toast.LENGTH_SHORT).show();
        }

    }
}
