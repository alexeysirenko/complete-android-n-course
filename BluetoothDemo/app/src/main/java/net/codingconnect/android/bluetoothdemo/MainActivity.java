package net.codingconnect.android.bluetoothdemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    BluetoothAdapter bluetoothAdapter;

    ListView devicesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        devicesListView = (ListView) findViewById(R.id.devicesListView);

        Toast.makeText(getApplicationContext(), "Bluetooth is " + (bluetoothAdapter.isEnabled() ? "ON" : "OFF"), Toast.LENGTH_SHORT).show();

    }

    public void onTurnOnButtonClick(View v) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivity(intent);
        if (bluetoothAdapter.isEnabled()) {
            Toast.makeText(getApplicationContext(), "Bluetooth turned on", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Failed to enable Bluetooth", Toast.LENGTH_SHORT).show();
        }
    }

    public void onTurnOffButtonClick(View v) {
        bluetoothAdapter.disable();
        if (!bluetoothAdapter.isEnabled()) {
            Toast.makeText(getApplicationContext(), "Bluetooth disabled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Failed to disable Bluetooth", Toast.LENGTH_SHORT).show();
        }
    }

    public void onListButtonClick(View v) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivity(intent);
    }

    public void onListPairedButtonClick(View v) {
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        ArrayList<String> devicesInfo = new ArrayList<>();
        for (BluetoothDevice device: pairedDevices) {
            devicesInfo.add(device.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, devicesInfo);

        devicesListView.setAdapter(adapter);

    }
}
