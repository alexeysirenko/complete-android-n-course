package com.parse.starter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        final ListView feedListView = (ListView) findViewById(R.id.feedListView);
        final List<Map<String, String>> tweets = new ArrayList<>();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tweet");

        query.whereContainedIn("userId", ParseUser.getCurrentUser().getList("followedUsersIds"));
        query.orderByDescending("createdAt");
        query.setLimit(20);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (ParseObject tweet: objects) {
                        Map<String, String> tweetInfo = new HashMap<>();
                        tweetInfo.put("username", tweet.getString("username"));
                        tweetInfo.put("content", tweet.getString("tweet"));
                        tweets.add(tweetInfo);
                    }
                    SimpleAdapter simpleAdapter = new SimpleAdapter(FeedActivity.this, tweets,
                            android.R.layout.simple_list_item_2,
                            new String[] {"content", "username"},
                            new int[] {android.R.id.text1, android.R.id.text2});
                    feedListView.setAdapter(simpleAdapter);
                }
            }
        });


    }

    /*private class Tweet {
        public final String userId;
        public final String username;
        public final String tweet;

        public Tweet(String userId, String username, String tweet) {
            this.userId = userId;
            this.username = username;
            this.tweet = tweet;
        }
    }*/
}
