package com.parse.starter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserListActivity extends AppCompatActivity {

    private final ArrayList<User> users = new ArrayList<>();

    private ListView usersListView;

    private ArrayAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        setTitle("User List");

        usersListView = (ListView) findViewById(R.id.userListView);

        usersListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

        userListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, users);

        usersListView.setAdapter(userListAdapter);

        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView checkedTextView = (CheckedTextView) view;
                User user = users.get(position);
                List<String> alreadyFollowedUsers = ParseUser.getCurrentUser().getList("followedUsersIds");
                Set<String> uniqueFollowedUsers = alreadyFollowedUsers != null ? new HashSet<>(alreadyFollowedUsers) : new HashSet<String>();
                if (checkedTextView.isChecked()) {
                    uniqueFollowedUsers.add(user.id);
                } else {
                    uniqueFollowedUsers.remove(user.id);
                }
                List<String> updatedUsers = new ArrayList<>(uniqueFollowedUsers);
                ParseUser.getCurrentUser().remove("followedUsersIds");
                ParseUser.getCurrentUser().addAll("followedUsersIds", updatedUsers);
                ParseUser.getCurrentUser().saveInBackground();
            }
        });

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    for (ParseUser user: objects) {
                        users.add(new User(user.getObjectId(), user.getUsername()));
                    }
                    userListAdapter.notifyDataSetChanged();
                    List<String> followedUsersIds = ParseUser.getCurrentUser().getList("followedUsersIds");
                    if (followedUsersIds != null && followedUsersIds.size() > 0) {
                        for (int i = 0; i < users.size(); i++) {
                            User user = users.get(i);
                            usersListView.setItemChecked(i, followedUsersIds.contains(user.id));
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            ParseUser.logOut();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else if (item.getItemId() == R.id.feed) {
            Intent intent = new Intent(getApplicationContext(), FeedActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onTweetButtonClick(View v) {
        final EditText tweetContentEditText = new EditText(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Send a tweet")
                .setView(tweetContentEditText)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ParseObject tweet = new ParseObject("Tweet");
                        tweet.put("userId", ParseUser.getCurrentUser().getObjectId());
                        tweet.put("username", ParseUser.getCurrentUser().getUsername());
                        tweet.put("tweet", tweetContentEditText.getText().toString());
                        tweet.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Toast.makeText(UserListActivity.this, "Tweeted", Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.e("Tweet", e.getMessage());
                                    Toast.makeText(UserListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    private class User {

        public final String id;
        public final String name;

        public User(String id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
