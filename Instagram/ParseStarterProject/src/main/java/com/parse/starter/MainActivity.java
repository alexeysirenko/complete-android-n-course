/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    EditText loginEditText;
    EditText passwordEditText;

    private AuthModeState authModeState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        passwordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    signup();
                    return true;
                }
                return false;
            }
        });

        RelativeLayout backgroundLayout = (RelativeLayout) findViewById(R.id.backgroundLayout);
        ImageView logoImageView = (ImageView) findViewById(R.id.logoImageView);
        List<View> backgroundViews = Arrays.asList(backgroundLayout, logoImageView);
        View.OnClickListener backgroundClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    manager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
                }
            }
        };
        for (View view: backgroundViews) {
            view.setOnClickListener(backgroundClickListener);
        }

        setSignupMode(AuthMode.LOGIN);

        if (ParseUser.getCurrentUser() != null) {
            startUsersActivity();
        }
    }

    private void startUsersActivity() {
        startActivity(new Intent(getApplicationContext(), UsersActivity.class));
    }

    public void onSignupClick(View v) {
        signup();
    }

    public void onSwitchToSignupClick(View v) {
        setSignupMode(AuthMode.SIGNUP);
    }

    public void onSwitchToLoginClick(View v) {
        setSignupMode(AuthMode.LOGIN);
    }

    private void setSignupMode(AuthMode mode) {
        TableRow signupRow = (TableRow) findViewById(R.id.signupRow);
        TableRow loginRow = (TableRow) findViewById(R.id.loginRow);
        switch (mode) {
            case SIGNUP:
                signupRow.setVisibility(View.VISIBLE);
                loginRow.setVisibility(View.GONE);
                authModeState = new SignupModeState();
                break;
            case LOGIN:
                signupRow.setVisibility(View.GONE);
                loginRow.setVisibility(View.VISIBLE);
                authModeState = new LoginModeState();
                break;
            default:
                Log.e("Signup mode", "Unknown signup mode " + mode);
        }
    }

    private void signup() {
        String username = loginEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString();
        authModeState.signup(username, password);
    }

    private enum AuthMode {
        LOGIN, SIGNUP
    }

    private abstract class AuthModeState {
        abstract void signup(String username, String password);

        protected boolean isValidLogin(String username) {
            return username.length() > 3;
        }

        protected boolean isValidPassword(String pasword) {
            return pasword.length() > 3;
        }
    }

    private class SignupModeState extends AuthModeState {

        @Override
        public void signup(String username, String password) {
            if (isValidLogin(username) && isValidPassword(password)) {
                ParseUser user = new ParseUser();
                user.setUsername(username);
                user.setPassword(password);
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.i("Signup", "Successful");
                            startUsersActivity();
                        } else {
                            Log.e("Signup", "Failed: " + e.getMessage());
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                if (!isValidLogin(username)) {
                    Toast.makeText(MainActivity.this, "Invalid username", Toast.LENGTH_SHORT).show();
                } else if (!isValidPassword(password)) {
                    Toast.makeText(MainActivity.this, "Invalid password", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public String toString() {
            return "SignupModeState";
        }
    }

    private class LoginModeState extends AuthModeState {

        @Override
        public void signup(String username, String password) {
            if (isValidLogin(username) && isValidPassword(password)) {
                ParseUser.logInInBackground(username, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            Log.i("Login", "Successful");
                            startUsersActivity();
                        } else {
                            Log.e("Login", "Failed: " + e.getMessage());
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                if (!isValidLogin(username)) {
                    Toast.makeText(MainActivity.this, "Invalid username", Toast.LENGTH_SHORT).show();
                } else if (!isValidPassword(password)) {
                    Toast.makeText(MainActivity.this, "Invalid password", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public String toString() {
            return "LoginModeState";
        }
    }
    
    

}
