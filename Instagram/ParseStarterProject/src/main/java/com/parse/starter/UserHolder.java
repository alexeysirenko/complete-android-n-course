package com.parse.starter;

import com.parse.ParseUser;

public class UserHolder {
    private String name;

    private String id;

    private ParseUser parseUser;

    private UserHolder() {}

    public UserHolder(ParseUser user) {
        this.parseUser = user;
        this.id = user.getObjectId();
        this.name = user.getUsername();
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public ParseUser getParseUser() {
        return parseUser;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
