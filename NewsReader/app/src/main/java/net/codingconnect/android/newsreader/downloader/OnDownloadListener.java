package net.codingconnect.android.newsreader.downloader;

/**
 * Created by Oleksiy on 28.04.2017.
 */
public interface OnDownloadListener {

    void onDownload(String data);

}