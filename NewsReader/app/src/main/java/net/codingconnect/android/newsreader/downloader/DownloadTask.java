package net.codingconnect.android.newsreader.downloader;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A task to download web content.
 */
public class DownloadTask {

    private String url;

    private final ArrayList<OnDownloadListener> listeners = new ArrayList<>();

    private DownloadTask() {}

    public DownloadTask(String url) {
        this.url = url;
    }

    public DownloadTask withListener(OnDownloadListener listener) {
        listeners.add(listener);
        return this;
    }

    public DownloadTask execute() {
        task.execute(url);
        return this;
    }

    private AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            for (OnDownloadListener listener: listeners) {
                listener.onDownload(result);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                URL url = new URL(params[0]);
                URLConnection urlConnection =  url.openConnection();
                InputStream in = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                try {
                    StringBuilder builder = new StringBuilder();
                    while (true) {
                        String inputLine = reader.readLine();
                        if (inputLine == null) break;
                        builder.append(inputLine);
                    }
                    result = builder.toString();
                } finally {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    };
}


