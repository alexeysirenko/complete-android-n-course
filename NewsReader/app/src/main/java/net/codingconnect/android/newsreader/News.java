package net.codingconnect.android.newsreader;

/**
 * News item.
 */
public class News {
    private int id;
    private String title;
    private String url;

    public News(int id, String title, String url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
