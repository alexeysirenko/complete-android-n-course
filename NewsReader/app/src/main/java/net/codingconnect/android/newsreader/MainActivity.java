package net.codingconnect.android.newsreader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.codingconnect.android.newsreader.downloader.DownloadTask;
import net.codingconnect.android.newsreader.downloader.OnDownloadListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    final ArrayList<News> newsList = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News news = newsList.get(position);
                Intent intent = new Intent(getApplicationContext(), NewsViewActivity.class);
                intent.putExtra("url", news.getUrl());
                startActivity(intent);
            }
        });

        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, newsList);

        listView.setAdapter(arrayAdapter);

        new DownloadTask("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
            .withListener(itemsListDownloadListener)
            .execute();
    }

    private OnDownloadListener itemsListDownloadListener = new OnDownloadListener() {
        @Override
        public void onDownload(String data) {
            try {
                JSONArray arr = new JSONArray(data);
                for (int i = 0; i < arr.length(); i++) {
                    final int id = Integer.parseInt(arr.get(i).toString());
                    new DownloadTask("https://hacker-news.firebaseio.com/v0/item/" + Integer.toString(id) + ".json?print=pretty")
                        .withListener(itemDetailsDownloadListener)
                        .execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private OnDownloadListener itemDetailsDownloadListener = new OnDownloadListener() {
        @Override
        public void onDownload(String data) {
            try {
                JSONObject response = new JSONObject(data);
                int id = response.getInt("id");
                String title = response.getString("title");
                String url = response.getString("url");
                newsList.add(new News(id, title, url));
                arrayAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
