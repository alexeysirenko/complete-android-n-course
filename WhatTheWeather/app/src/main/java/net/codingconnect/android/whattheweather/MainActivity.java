package net.codingconnect.android.whattheweather;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    TextView weatherDetailsView = null;
    EditText cityEditText = null;
    Button getWeatherButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherDetailsView = (TextView) findViewById(R.id.weatherDetailsView);
        cityEditText = (EditText) findViewById(R.id.cityEditText);
        getWeatherButton = (Button) findViewById(R.id.getWeatherButton);
    }

    public void onGetWeatherClick(View v) {
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(cityEditText.getWindowToken(), 0);
        try {
            String cityName = URLEncoder.encode(cityEditText.getText().toString(), "UTF-8");
            DownloadTask task = new DownloadTask();
            task.execute(cityName);
            getWeatherButton.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to get weather data", Toast.LENGTH_SHORT).show();
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... cityNames) {
            String api = "http://api.openweathermap.org/data/2.5/weather?appid=2e68e2962a90b245cbbf09b01dbdb654&q=" + cityNames[0];
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(api);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                while (true) {
                    int data = reader.read();
                    if (data < 0) break;
                    char current = (char) data;
                    result += current;
                }

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            weatherDetailsView.setText("");
            try {
                JSONObject jsonObject = new JSONObject(result);
                String weatherInfo = jsonObject.getString("weather");
                Log.i("Website content", weatherInfo);

                JSONArray arr = new JSONArray(weatherInfo);
                String detailsText = "City: " + cityEditText.getText().toString() + "\r\n";

                for (int i = 0; i < arr.length(); i++) {
                    JSONObject jsonPart = arr.getJSONObject(i);
                    String main =  jsonPart.getString("main");
                    String description =  jsonPart.getString("description");

                    if (!main.isEmpty() && !description.isEmpty()) {
                        detailsText += main + ": " + description + "\r\n";
                    }
                }
                weatherDetailsView.setText(detailsText);
                getWeatherButton.setEnabled(true);
            } catch (JSONException e) {
                e.printStackTrace();
                weatherDetailsView.setText(e.getMessage());
            }
        }
    }

}
