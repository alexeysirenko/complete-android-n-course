package net.codingconnect.android.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.apache.commons.lang3.SerializationUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = this.getSharedPreferences("foo.bar.SharedPreferences", Context.MODE_PRIVATE);

        String username = sharedPreferences.getString("username", "");
        Log.i("username", username);
        sharedPreferences.edit().putString("username", "Foo").apply();

        username = sharedPreferences.getString("username", "");
        Log.i("username", username);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Chandler");
        friends.add("Monica");
        friends.add("Joey");
        friends.add("Ross");

        /// Serialize and deserialize as JSON
        JSONArray jsonArray = new JSONArray(friends);
        Log.i("friends", jsonArray.toString());

        sharedPreferences.edit().putString("friends", jsonArray.toString()).apply();

        try {
            JSONArray arr = new JSONArray(sharedPreferences.getString("friends", "[]"));
            friends.clear();
            for (int i = 0; i < arr.length(); i++) {
                friends.add(arr.get(i).toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Friends loaded", friends.toString());


        /// Serialize and deserialize with serializer class
        try {
            sharedPreferences.edit().putString("friends", ObjectSerializer.serialize(friends)).apply();
        } catch (IOException e) {
            e.printStackTrace();
        }

        friends.clear();

        try {
            friends = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("friends", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Log.i("Friends loaded2", friends.toString());
    }
}
