/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseUser user = new ParseUser();
        user.setUsername("rob");
        user.setPassword("password");

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("Signup", "Success");
                } else {
                    Log.i("Signup", "Failed: " + e.getMessage());
                }
            }
        });


        ParseUser.logInInBackground("rob", "password", new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    Log.i("Login", "Successful");
                } else {
                    Log.i("Login", "Failed: " + e.getMessage());
                }
            }
        });

        if (ParseUser.getCurrentUser() != null) {
            Log.i("CurrentUser", "User logged in: " + ParseUser.getCurrentUser().getUsername());
        } else {
            Log.i("CurrentUser", "Not logged");
        }

        ParseUser.logOut();

        /*ParseObject score = new ParseObject("Score");
        score.put("foo", "bar");
        score.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("SaveInBackground", "Success");
                } else {
                    Log.i("SaveInBackground", e.getMessage());
                }
            }
        });*/

        ParseQuery<ParseObject> query0 = ParseQuery.getQuery("Score");
        //query0.whereEqualTo("username", "charly");
        query0.whereGreaterThan("score", 100);
        query0.setLimit(1);

        query0.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null && objects != null) {
                    Log.i("Find", objects.size() + " objects");

                    for (ParseObject object : objects) {
                        Log.i("Find", object.getString("username"));

                        object.put("score", object.getInt("score") + 50);
                        object.saveInBackground();
                    }
                }
            }
        });

        /*ParseQuery<ParseObject> query = ParseQuery.getQuery("Score");

        query.getInBackground("dadad", new GetCallback<ParseObject>() {

            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null && object != null) {

                    object.put("score", 200);
                    object.saveInBackground();

                    Log.i("ObjectValue", object.getString("username"));
                }
            }
        });


        ParseObject tweet = new ParseObject("Tweet");

        tweet.put("username", "foo");
        tweet.put("tweet", "Hey there");
        tweet.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("SaveInBackground", "Success");
                } else {
                    Log.i("SaveInBackground", e.getMessage());
                }
            }
        });*/


        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

}
