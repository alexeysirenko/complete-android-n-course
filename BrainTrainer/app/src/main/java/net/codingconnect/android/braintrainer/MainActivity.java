package net.codingconnect.android.braintrainer;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private LinearLayout controlsLayout;
    private Button goButton;
    private Button startButton;
    private TextView timeLeftTextView;
    private TextView messageTextView;
    private TextView resultTextView;
    private TextView questionTextView;
    private List<Button> answerButtons = new ArrayList<>();

    private CountDownTimer timer = null;

    private final int INTERVAL = 60 * 1000;

    private int guessedNumber = 0;
    private int totalAnswers = 0;
    private int correctAnswers = 0;

    private final Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controlsLayout = (LinearLayout) findViewById(R.id.controlsLayout);
        goButton = (Button) findViewById(R.id.goButton);
        startButton = (Button) findViewById(R.id.startButton);
        timeLeftTextView = (TextView) findViewById(R.id.timeLeftTextView);
        messageTextView = (TextView) findViewById(R.id.messageTextView);
        resultTextView = (TextView) findViewById(R.id.resultTextView);
        questionTextView = (TextView) findViewById(R.id.questionTextView);

        answerButtons.clear();
        answerButtons.add((Button) findViewById(R.id.answerButton1));
        answerButtons.add((Button) findViewById(R.id.answerButton2));
        answerButtons.add((Button) findViewById(R.id.answerButton3));
        answerButtons.add((Button) findViewById(R.id.answerButton4));

        controlsLayout.setVisibility(View.GONE);
        goButton.setVisibility(View.VISIBLE);
        initScore();
        stopGame();
    }

    public void goButtonClick(View view) {
        controlsLayout.setVisibility(View.VISIBLE);
        goButton.setVisibility(View.GONE);
        startGame();
    }

    public void startButtonClick(View view) {
        if (timer == null) {
            startGame();
        } else {
            stopGame();
        }
    }

    private void initScore() {
        guessedNumber = 0;
        totalAnswers = 0;
        correctAnswers = 0;
    }

    public void answerButtonClick(View view) {
        if (timer != null) {
            int answer = Integer.parseInt(view.getTag().toString());
            totalAnswers++;
            if (answer == guessedNumber) {
                correctAnswers++;
                messageTextView.setText(R.string.correctMessage);
            } else {
                messageTextView.setText(String.format(getString(R.string.wrongMessage), Integer.toString(guessedNumber)));
            }
            displayScore();
            guessNextNumber();
        }
    }

    private void startGame() {
        if (timer != null) {
            timer.cancel();
        }
        enableButtons(true);
        initScore();
        displayScore();
        timer = new CountDownTimer(INTERVAL, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                displayTimeout(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                stopGame();
            }
        };
        timer.start();
        startButton.setText(R.string.stop);
        guessNextNumber();
    }

    private void stopGame() {
        if (timer != null) {
            timer.cancel();
        }
        timer = null;
        enableButtons(false);
        displayTimeout(0);
        startButton.setText(R.string.start);
        messageTextView.setText(String.format(getString(R.string.yourScoreIs), correctAnswers * 10));
    }

    private void guessNextNumber() {
        int[] summands = {10 + random.nextInt(50), 10 + random.nextInt(50)};
        //Arrays.sort(summands);
        int result = summands[1] + summands[0];

        String displayText = Integer.toString(summands[1]) + " + " + Integer.toString(summands[0]);
        questionTextView.setText(displayText);

        for (Button button: answerButtons) {
            // randomly fill buttons with near-to-answer numbers
            int randomNumber = result;
            while (randomNumber == result) {
                randomNumber = result - 10 + random.nextInt(20);
                button.setText(Integer.toString(randomNumber));
                button.setTag(Integer.toString(randomNumber));
            }
        }

        Button correctButton = answerButtons.get(random.nextInt(answerButtons.size()));
        correctButton.setText(Integer.toString(result));
        correctButton.setTag(Integer.toString(result));

        guessedNumber = result;
    }

    private void displayScore() {
        resultTextView.setText(Integer.toString(correctAnswers) + "/" + Integer.toString(totalAnswers));
    }

    private void displayTimeout(long millisUntilFinished) {
        String text = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));
        timeLeftTextView.setText(text);
    }

    private void enableButtons(boolean enabled) {
        for (Button button: answerButtons) {
            button.setEnabled(enabled);
        }
    }
}
