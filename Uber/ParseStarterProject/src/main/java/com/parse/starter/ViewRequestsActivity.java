package com.parse.starter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ViewRequestsActivity extends AppCompatActivity {

    ListView requestsListView;

    ArrayList<Request> requests = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    private LocationManager locationManager;

    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_requests);

        setTitle("Nearby requests");

        requestsListView = (ListView) findViewById(R.id.requestsListView);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, requests);

        requestsListView.setAdapter(arrayAdapter);

        requestsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDriverLocation(requests.get(position));
            }
        });

        if (ParseUser.getCurrentUser() == null) {
            Utils.login(this);
            finish();
        } else {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(@NonNull Location location) {
                    Log.i("Location changed", location.toString());
                    update(location);
                    saveDriverLocation(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                update(Utils.getLastKnownLocation(this, locationManager));
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // ask for permission
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[]permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                update(Utils.getLastKnownLocation(this, locationManager));
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    private void update(Location location) {
        if (location != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
            final ParseGeoPoint geoPoint = new ParseGeoPoint(location.getLatitude(), location.getLongitude());

            query.whereNear("location", geoPoint);
            query.whereDoesNotExist("driverId");
            query.setLimit(10);

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        requests.clear();
                        for (ParseObject object: objects) {
                            ParseGeoPoint location = (ParseGeoPoint) object.get("location");
                            if (location != null) {
                                Double distance = Math.round(geoPoint.distanceInKilometersTo(location) * 10.0) / 10.0;
                                Request request = new Request();
                                request.distance = distance;
                                request.id = object.getObjectId();
                                request.lat = location.getLatitude();
                                request.lng = location.getLongitude();
                                requests.add(request);
                            }
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }

    private void showDriverLocation(Request request) {
        Location lastKnownLocation = Utils.getLastKnownLocation(this, locationManager);
        if (lastKnownLocation != null) {
            Intent intent = new Intent(getApplicationContext(), DriverLocationActivity.class);
            intent.putExtra("requestLat", request.lat);
            intent.putExtra("requestLng", request.lng);
            intent.putExtra("driverLat", lastKnownLocation.getLatitude());
            intent.putExtra("driverLng", lastKnownLocation.getLongitude());
            intent.putExtra("requestId", request.id);
            startActivity(intent);
        }
    }

    private void saveDriverLocation(Location location) {
        ParseUser.getCurrentUser().put("location", new ParseGeoPoint(location.getLatitude(), location.getLongitude()));
        ParseUser.getCurrentUser().saveInBackground();
    }

    private class Request {
        String id;
        Double distance;
        Double lat;
        Double lng;

        @Override
        public String toString() {
            return distance.toString() + " km";
        }
    }
}
