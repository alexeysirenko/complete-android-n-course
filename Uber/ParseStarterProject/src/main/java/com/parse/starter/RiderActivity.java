package com.parse.starter;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class RiderActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private Marker marker = null;

    private LocationManager locationManager;

    private LocationListener locationListener;

    private Button callUberButton;

    private TextView infoTextView;

    private Handler handler = new Handler();

    private @Nullable ParseObject activeUberRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider);

        callUberButton = (Button) findViewById(R.id.callButton);

        infoTextView = (TextView) findViewById(R.id.infoTextView);

        if (ParseUser.getCurrentUser() == null) {
            Utils.login(this);
            finish();
        } else {
            checkActiveRequest();

            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(@NonNull Location location) {
                    Log.i("Location changed", location.toString());
                    if (marker == null) {
                        centerMapOnLocation(location.getLatitude(), location.getLongitude());
                        setMarker(new LatLng(location.getLatitude(), location.getLongitude()));
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activeUberRequest != null) {
                    checkForUpdates();
                }
            }
        }, 2000);
    }

    public void onCallUberButtonClick(View v) {
        if (activeUberRequest == null) {
            if (marker != null) {
                callUber();
            } else {
                Toast.makeText(this, "Mark your location", Toast.LENGTH_SHORT).show();
            }
        } else {
            cancelUber();
        }
    }

    public void onLogoutButtonClick(View v) {
        ParseUser.logOut();
        Utils.login(this);
        finish();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear();
                marker = null;
                setMarker(latLng);
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            goToLastKnownLocation();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // ask for permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    private void checkActiveRequest() {
        ParseQuery<ParseObject> query = new ParseQuery<>("Request");
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        activeUberRequest = objects.get(0);
                        callUberButton.setText(R.string.cancelUber);
                        Log.i("Uber Request", activeUberRequest.toString());
                    } else {
                        activeUberRequest = null;
                        callUberButton.setText(R.string.callUber);
                        Log.i("Uber Request", "No active requests");
                    }
                } else {
                    Log.e("Uber Request", e.getMessage());
                    Toast.makeText(getApplicationContext(), "Failed to make an Uber request: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callUber() {
        if (marker != null) {
            final ParseObject request = new ParseObject("Request");
            final String user = ParseUser.getCurrentUser().getUsername();
            request.put("username", user);
            ParseGeoPoint parseGeoPoint = new ParseGeoPoint(marker.getPosition().latitude, marker.getPosition().longitude);
            request.put("location", parseGeoPoint);
            request.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        activeUberRequest = request;
                        callUberButton.setText(R.string.cancelUber);
                        Log.i("Uber Request", "Uber has been called: " + user + " " +  marker.getPosition().toString());
                    } else {
                        Log.e("Uber Request", e.getMessage());
                        Toast.makeText(getApplicationContext(), "Failed to make an Uber request: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            Log.e("Uber Request", "Unable to find your location");
            Toast.makeText(getApplicationContext(), "Unable to find your location", Toast.LENGTH_SHORT).show();
        }
    }

    private void cancelUber() {
        ParseQuery<ParseObject> query = new ParseQuery<>("Request");
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (ParseObject object: objects) {
                        object.deleteInBackground();
                    }
                    activeUberRequest = null;
                    callUberButton.setText(R.string.callUber);
                    Log.i("Cancel Uber Request", "All requests are cancelled");
                } else {
                    Log.e("Cancel Uber Request", e.getMessage());
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[]permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                goToLastKnownLocation();
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    private void goToLastKnownLocation() {
        Location lastKnownLocation = Utils.getLastKnownLocation(this, locationManager);
        if (lastKnownLocation != null) {
            locationListener.onLocationChanged(lastKnownLocation);
        }
    }

    private void centerMapOnLocation(double lat, double lng) {
        LatLng latLng = new LatLng(lat, lng);
        mMap.clear();
        setMarker(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    private void setMarker(LatLng latLng) {
        String title = "Your location";
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                if (addressList.get(0).getThoroughfare() != null) {
                    title = addressList.get(0).getThoroughfare() + " " + addressList.get(0).getSubThoroughfare();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        marker = mMap.addMarker(new MarkerOptions().position(latLng).title(title));
    }

    private void checkForUpdates() {
        if (activeUberRequest != null) {
            final ParseQuery<ParseObject> query = ParseQuery.getQuery("Request");
            query.whereEqualTo("objectId", activeUberRequest.getObjectId());
            query.whereExists("driverId");
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        if (objects.size() > 0) {
                            infoTextView.setText(R.string.driver_on_the_way);
                            infoTextView.setVisibility(View.VISIBLE);
                            callUberButton.setVisibility(View.INVISIBLE);

                            ParseQuery<ParseUser> query = ParseUser.getQuery();
                            query.whereEqualTo("objectId", objects.get(0).get("driverId"));
                            query.findInBackground(new FindCallback<ParseUser>() {
                                @Override
                                public void done(List<ParseUser> objects, ParseException e) {
                                    if (e == null && objects.size() > 0) {
                                        ParseGeoPoint driverLocationPoint = objects.get(0).getParseGeoPoint("location");
                                        Location riderLocation = Utils.getLastKnownLocation(getApplicationContext(), locationManager);
                                        if (riderLocation != null) {
                                            ParseGeoPoint riderLocationPoint = new ParseGeoPoint(riderLocation.getLatitude(), riderLocation.getLongitude());
                                            Double distance = Math.round(riderLocationPoint.distanceInKilometersTo(driverLocationPoint) * 10.0) / 10.0;
                                            if (distance > 0.01) {
                                                infoTextView.setText(String.format(Locale.ENGLISH, getString(R.string.driver_distance), distance));
                                            } else { // Driver is here
                                                infoTextView.setText(R.string.driver_arrived);
                                                // Clears related requests
                                                cancelUber();
                                                mMap.clear();
                                            }
                                        }
                                    }
                                }
                            });

                        } else {
                            infoTextView.setVisibility(View.INVISIBLE);
                            callUberButton.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        } else {
            infoTextView.setVisibility(View.INVISIBLE);
            callUberButton.setVisibility(View.VISIBLE);
        }
    }
}
