/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.parse.ParseAnalytics;
import com.parse.ParseUser;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        if (ParseUser.getCurrentUser() == null) {
            Utils.login(this);
        } else {
            boolean isDriver = (Boolean) ParseUser.getCurrentUser().get("isDriver");
            Log.i("Current user", isDriver ? "driver" : "rider");
            if (isDriver) {
                startActivity(new Intent(getApplicationContext(), ViewRequestsActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), RiderActivity.class));
            }
        }
        finish();
    }

}
