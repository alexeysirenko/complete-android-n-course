package com.parse.starter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

/**
 * Created by Oleksiy on 10.05.2017.
 */
public class Utils {

    public static void login(Context context) {
        context.startActivity(new Intent(context.getApplicationContext(), LoginActivity.class));
    }

    public static @Nullable Location getLastKnownLocation(Context context, LocationManager locationManager) {
        Location location = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        return location;
    }

}
