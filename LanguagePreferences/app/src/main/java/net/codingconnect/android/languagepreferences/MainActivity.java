package net.codingconnect.android.languagepreferences;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);

        sharedPreferences = this.getSharedPreferences("foo.bar.LanguagePreferences", Context.MODE_PRIVATE);

        String language = sharedPreferences.getString("language", null);

        if (language != null) {
            setLanguage(language);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Language")
                    .setMessage("Select your language")
                    .setPositiveButton("English", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setLanguage("eng");
                        }
                    })
                    .setNegativeButton("Spanish", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setLanguage("spn");
                        }
                    })
                    .show();
        }
    }

    private void setLanguage(String language) {
        switch (language) {
            case "spn":
                textView.setText("Spanish");
                break;
            default:
                textView.setText("English");
        }
        sharedPreferences.edit().putString("language", language).apply();
    }
}
