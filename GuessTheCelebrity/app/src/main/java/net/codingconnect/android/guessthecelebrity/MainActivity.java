package net.codingconnect.android.guessthecelebrity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> celebrityUrls = new ArrayList<>();
    private ArrayList<String> celebrityNames = new ArrayList<>();
    private int chosenCelebrity = 0;
    private int locationOfCorrectAnswer = 0;
    String[] answers = new String[4];
    Button[] buttons = new Button[4];

    ImageView imageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);

        buttons[0] = (Button) findViewById(R.id.choseButton1);
        buttons[1] = (Button) findViewById(R.id.choseButton2);
        buttons[2] = (Button) findViewById(R.id.choseButton3);
        buttons[3] = (Button) findViewById(R.id.choseButton4);

        nextQuestion();
    }

    public void onChosenClick(View view) {
        Log.i("Answer", view.getTag() + " - " + Integer.toString(locationOfCorrectAnswer));
        if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {
            Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Wrong. It was " + celebrityNames.get(chosenCelebrity), Toast.LENGTH_SHORT).show();
        }
        nextQuestion();
    }

    private void nextQuestion() {
        DownloadTask task = new DownloadTask();
        try {
            String result = task.execute("http://www.posh24.se/kandisar").get();

            String[] splitResult = result.split("<div class=\"sidebarContainer\">");

            final Pattern srcPattern = Pattern.compile("<img src=\"(.*?)\"");
            final Matcher srcMatcher = srcPattern.matcher(splitResult[0]);

            while (srcMatcher.find()) {
                celebrityUrls.add(srcMatcher.group(1));
            }

            final Pattern altPattern = Pattern.compile("alt=\"(.*?)\"");
            final Matcher altMatcher = altPattern.matcher(splitResult[0]);

            while (altMatcher.find()) {
                celebrityNames.add(altMatcher.group(1));
            }

            if (celebrityNames.size() > 0) {
                Random random = new Random();
                int retries = 3;
                while (retries > 0) {
                    retries--;
                    chosenCelebrity = random.nextInt(celebrityUrls.size());
                    ImageDownloader imageTask = new ImageDownloader();
                    Bitmap celebImage = imageTask.execute(celebrityUrls.get(chosenCelebrity)).get();
                    if (celebImage != null) {
                        imageView.setImageBitmap(celebImage);
                        locationOfCorrectAnswer = random.nextInt(4);

                        for (int i = 0; i < 4; i++) {
                            if (i == locationOfCorrectAnswer) {
                                answers[i] = celebrityNames.get(chosenCelebrity);
                            } else {
                                int r = 3;
                                int incorrectAnswerLocation = 0;
                                while (r > 0) {
                                    r--;
                                    incorrectAnswerLocation = random.nextInt(celebrityUrls.size());
                                    if (incorrectAnswerLocation != locationOfCorrectAnswer) {
                                        break;
                                    }
                                }
                                answers[i] = celebrityNames.get(incorrectAnswerLocation);
                            }
                        }

                        break;
                    }
                }


            }

            for (int i = 0; i < buttons.length && i < answers.length; i++) {
                buttons[i].setText(answers[i]);
            }

            Log.i("", result);
        } catch(InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                InputStreamReader reader = new InputStreamReader(inputStream);

                StringBuilder resultBuilder = new StringBuilder();
                while (true) {
                    int data = reader.read();
                    if (data < 0) break;
                    char current = (char) data;
                    resultBuilder.append(current);
                }
                return resultBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                return BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
